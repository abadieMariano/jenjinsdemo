package Jenkins_Demo.Jenkins_Demo;
import org.junit.Assert;
import org.junit.Test;

public class CalculadoraTest{
	private Calculadora calculadora = new Calculadora();
	
	@Test
	public void testSuma(){
		int suma = this.calculadora.suma(2, 2);
		Assert.assertEquals(4,suma);
	}
	
	@Test
	public void testMultiplicacion(){
		int multiplicacion = this.calculadora.multiplicacion(2, 2);
		Assert.assertEquals(4,multiplicacion);
	}
	
	@Test
	public void testDivision(){
		int division = this.calculadora.division(2, 2);
		Assert.assertEquals(1,division);
	}
}
