package Jenkins_Demo.Jenkins_Demo;

public class Calculadora {

	public int suma(int value1, int value2) {
		return value1 + value2;
	}

	public int multiplicacion(int value1, int value2) {
		return value1 * value2;
	}

	public int division(int value1, int value2) {
		return value1 / value2;
	}

}
